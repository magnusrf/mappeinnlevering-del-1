package student.ntnu.idatt2001.magnusfarstad.hospital;

import student.ntnu.idatt2001.magnusfarstad.hospital.exception.RemoveException;

/**
 * Client for the application. The main method tests if
 * add to a department was successful and demonstrates the
 * remove method, both with legal and illegal input.
 */
public class HospitalClient {
    public static void main(String[] args) {

        Hospital hospital = new Hospital("NTNU Hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);

        Patient nonExistingPatient = new Patient("Joar", "Gunnarsen", "06608019463");
        Employee existingEmployee = new Employee("Terje", "Hutten", "24458852796");

        if(hospital.getDepartments().get("Akutten").addEmployee(existingEmployee)) {
            System.out.println("Employee " + existingEmployee.fullName() +  " was successfully added to Akutten");
        } else System.out.println("Employee was not successfully added to Akutten");

        System.out.println(hospital.toString());

        System.out.println("Removing " + existingEmployee.fullName() + " from Akutten");
        try {
            hospital.getDepartments().get("Akutten").remove(existingEmployee);
        } catch (RemoveException exception) {
            System.out.println(exception.getMessage());
        } if(!(hospital.getDepartments().get("Akutten").getEmployees().containsKey(existingEmployee.personId()))) {
            System.out.println("Removal was successful");
        }

        System.out.println();
        System.out.println("Removing " + nonExistingPatient.fullName() + " from Barn poliklinikk");
        try {
            hospital.getDepartments().get("Barn poliklinikk").remove(nonExistingPatient);
        } catch (RemoveException exception) {
            System.out.println(exception.getMessage());
        }

        System.out.println();
        System.out.println(hospital.toString());
    }
}
