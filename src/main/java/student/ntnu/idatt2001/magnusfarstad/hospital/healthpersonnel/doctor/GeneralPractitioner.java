package student.ntnu.idatt2001.magnusfarstad.hospital.healthpersonnel.doctor;

import student.ntnu.idatt2001.magnusfarstad.hospital.Patient;

public class GeneralPractitioner extends Doctor{

    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

    @Override
    public String toString() {
        return " General practitioner " + "\n";
    }
}
