package student.ntnu.idatt2001.magnusfarstad.hospital;
import java.util.HashMap;


public class Hospital {
    private final String HOSPITAL_NAME;
    private HashMap<String, Department> departments;

    public Hospital(String hospitalName) {
        this.HOSPITAL_NAME = hospitalName;
        this.departments = new HashMap<>();
    }

    public String getHOSPITAL_NAME() {
        return HOSPITAL_NAME;
    }

    public HashMap<String, Department> getDepartments() {
        return departments;
    }

    /**
     * Method responsible for adding a department to the HashMap register.
     * It checks if the department already exists by the name. If not
     * it adds the department to the register.
     * @param department The department to be added to the register
     */
    public void addDepartment(Department department) {
        if(!(this.departments.containsKey(department.getDepartmentName()))) {
            this.departments.put(department.getDepartmentName(), department);
        }
    }

    @Override
    public String toString() {
        return "Hospital: " +
                HOSPITAL_NAME +
                "\n\n" + departments + "\n\n";
    }
}
