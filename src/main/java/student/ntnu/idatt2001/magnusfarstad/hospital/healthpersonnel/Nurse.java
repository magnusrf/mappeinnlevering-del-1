package student.ntnu.idatt2001.magnusfarstad.hospital.healthpersonnel;

import student.ntnu.idatt2001.magnusfarstad.hospital.Employee;

public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return " Nurse " + "\n";
    }
}
