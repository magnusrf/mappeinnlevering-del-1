package student.ntnu.idatt2001.magnusfarstad.hospital;

/**
 * In addition to the attributes from Person,
 * Patient has/can have a diagnosis. It implements
 * the interface Diagnosable.
 */
public class Patient extends Person implements Diagnosable{
    private String diagnosis = "";

    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    protected String getDiagnose() {
        return this.diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        if(this.diagnosis.equals("")) {
            return " Patient " + "\n";
        }else return " Patient with diagnosis " + diagnosis + "\n";

    }
}
