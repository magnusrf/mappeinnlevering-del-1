package student.ntnu.idatt2001.magnusfarstad.hospital;

import student.ntnu.idatt2001.magnusfarstad.hospital.healthpersonnel.Nurse;
import student.ntnu.idatt2001.magnusfarstad.hospital.healthpersonnel.doctor.GeneralPractitioner;
import student.ntnu.idatt2001.magnusfarstad.hospital.healthpersonnel.doctor.Surgeon;

public final class HospitalTestData {

    private HospitalTestData() {
        // not called
    }
    /** Method filling the registers with test data. Some of the Persons have
     * a social security number, others not. This is for testing purposes and
     * shows that a person is identified by both full name and the social
     * security number.
     * @param hospital Is the object (Hospital) the test objects will be placed in.
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments

        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("Odd Even", "Primtallet", ""));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "48592058372"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", ""));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", ""));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "85930447123"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", ""));
        emergency.addEmployee(new Nurse("Ove", "Ralt", ""));
        emergency.addPatient(new Patient("Inga", "Lykke", "65738903987"));
        emergency.addPatient(new Patient("Ulrik", "Smål", ""));
        hospital.getDepartments().put(emergency.getDepartmentName(), emergency);

        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "84920395856"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", ""));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", ""));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", ""));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", ""));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "84029384756"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", ""));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", ""));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", ""));
        hospital.getDepartments().put(childrenPolyclinic.getDepartmentName(), childrenPolyclinic);
    }
}
