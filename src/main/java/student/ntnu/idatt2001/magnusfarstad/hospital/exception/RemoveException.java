package student.ntnu.idatt2001.magnusfarstad.hospital.exception;

/**
 * RemoveException is a custom exception, being thrown
 * when method remove() in Department fails to remove a
 * person.
 */
public class RemoveException extends Throwable{
    private static final long serialVersionUiD = 1L;

    public RemoveException(String message) {
        super(message);
    }
}
