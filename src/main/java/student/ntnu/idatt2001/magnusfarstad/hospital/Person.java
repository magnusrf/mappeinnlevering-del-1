package student.ntnu.idatt2001.magnusfarstad.hospital;

/**
 * Super class for all classes representing a person.
 * Every instance of a subclass of Person has a first name,
 * last name and a social security number.
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Method responsible for making of the full name
     * of a person.
     * @return Returns name on format "(first name) (last name)".
     */
    public String fullName() {
        return this.firstName + " " + this.lastName;
    }

    /**
     * A method responsible for making the person ID.
     * A person ID consists of the full name and the
     * social security number.
     * @return Returns the full name + social security number
     */
    public String personId() {
        return fullName() + " " + getSocialSecurityNumber() + " ";
    }

    @Override
    public String toString() {
        return " Person " + "\n";
    }
}
