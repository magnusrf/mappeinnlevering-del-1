package student.ntnu.idatt2001.magnusfarstad.hospital;
import student.ntnu.idatt2001.magnusfarstad.hospital.exception.RemoveException;

import java.util.HashMap;
import java.util.Objects;

/**
 * The department class is where all subclasses of Person
 * is registered. It consists of a name, a list of employees
 * and a list of patients.
 */
public class Department {
    private String departmentName;
    private HashMap<String, Employee> employees;
    private HashMap<String, Patient> patients;

    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new HashMap<>();
        this.patients = new HashMap<>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public HashMap<String, Employee> getEmployees() {
        return employees;
    }

    /**
     * Method adding employees to the register in the department.
     * It only adds the employee if he/she does not exist in the register
     * already, by checking if the persons id (full name and
     * social security number) is a key in the HashMap or not.
     * @param employee The employee to be added. This can be
     *                 a nurse, surgeon, general practitioner
     *                 or an employee.
     * @return {@code true} if add was successful,
     *         {@code false} if employee already exists in the register.
     */
    public boolean addEmployee(Employee employee) {
        Boolean status = false;
        if(!(this.employees.containsKey(employee.personId()))) {
            this.employees.put(employee.personId(), employee);
            status = true;
        }
        return status;
    }

    public HashMap<String, Patient> getPatients() {
        return patients;
    }

    /**
     * Method adding patients to the register in the department.
     * It only adds the patient if he/she does not exist in the register
     * already, by checking if the persons id (full name and social
     * security number) is a key in the HashMap or not.
     * @param patient The patient to be added.
     * @return {@code true} if add was successful,
     *         {@code false} if patient already exists in the register.
     */
    public boolean addPatient(Patient patient) {
        Boolean status = false;
        if(!(this.patients.containsKey(patient.personId()))) {
            this.patients.put(patient.personId(), patient);
            status = true;
        }
        return status;
    }

    /**
     * Method removing a person from the register it is in.
     * First it checks for null input.
     * Then it checks if the person is in one of the two HashMaps
     * employees or patients. If he/she is, then it removes
     * this person from the HashMap, thus removing him/her from
     * the department.
     * @param person Person to be removed from department.
     * @throws RemoveException if the person to be removed
     *         does not exist in either of the HashMaps or
     *         if person is null.
     */
    public void remove(Person person) throws RemoveException {
        if(person == null) {
            throw new RemoveException("Cannot remove NULL from department");
        } else {
            if(this.employees.containsKey(person.personId()) || this.patients.containsKey(person.personId())) {
                if(person instanceof Employee) {
                    this.employees.remove(person.personId(), person);
                }else this.patients.remove(person.personId(), person);
            } else throw new RemoveException("This person is not in any department");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) &&
                Objects.equals(employees, that.employees) &&
                Objects.equals(patients, that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    @Override
    public String toString() {
        return "Department: " + departmentName + '\'' + "\n" +
                " Employees: " + "\n" + employees + "\n" +
                " Patients: " + "\n" + patients + "\n";
    }
}
