package student.ntnu.idatt2001.magnusfarstad.hospital;

public interface Diagnosable {
    public abstract void setDiagnosis(String diagnosis);
}
