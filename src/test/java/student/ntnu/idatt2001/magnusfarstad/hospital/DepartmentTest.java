package student.ntnu.idatt2001.magnusfarstad.hospital;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import student.ntnu.idatt2001.magnusfarstad.hospital.exception.RemoveException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for method remove() in Department.java.
 * This validates the method.
 */
@DisplayName("Department test class")
public class DepartmentTest {

    @DisplayName("Successfully removes a person from a department")
    @Nested
    class removesSuccessfully {


        @DisplayName("Successfully removes an employee from a department")
        @Test
        public void removeEmployeeFromDepartment() {
            Department department = new Department("Psykologisk avdeling");
            Employee employee = new Employee("Gunn", "Ulriksen", "22018712345");
            department.addEmployee(employee);
            try {
                department.remove(employee);
            } catch (RemoveException exception) {
                exception.printStackTrace();
            } finally {
                assertFalse(department.getEmployees().containsKey(employee.personId()));
            }
        }

        @DisplayName("Successfully removes a patient from a department")
        @Test
        public void removePatientFromDepartment() {
            Department department = new Department("Akutten");
            Patient patient = new Patient("Joar", "Knudsen", "23035898765");
            department.addPatient(patient);
            try {
                department.remove(patient);
            } catch (RemoveException exception) {
                exception.printStackTrace();
            } finally {
                assertFalse(department.getPatients().containsKey(patient.personId()));
            }
        }
    }

    @DisplayName("Successfully handles illegal input")
    @Nested
    class removesUnsuccessfully {

        @DisplayName("Successfully handles remove on employee not registered")
        @Test
        public void unsuccessfullyRemoveEmployeeFromDepartment() {
            Department department = new Department("Avdeling");
            Employee employee = new Employee("Trond", "Fisk", "12066612345");

            try {
                department.remove(employee);
            } catch(RemoveException exception) {
                assertEquals(exception.getMessage(), "This person is not in any department");
            }
        }

        @DisplayName("Successfully handles remove on patient not registered")
        @Test
        public void unsuccessfullyRemovePatientFromDepartment() {
            Department department = new Department("Akutten");
            Patient patient = new Patient("Steinar", "Torgersen", "12126875648");

            try {
                department.remove(patient);
            } catch(RemoveException exception) {
                assertEquals(exception.getMessage(), "This person is not in any department");
            }
        }

        @DisplayName("Successfully handles null input")
        @Test
        public void successfullyHandlesNullInput() {
            Department department = new Department("Barneavdeling");
            try {
                department.remove(null);
            } catch (RemoveException exception) {
                assertEquals(exception.getMessage(), "Cannot remove NULL from department");
            }
        }
    }
}
